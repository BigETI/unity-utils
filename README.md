# Unity utilities package

## Description
This repository contains the utilities package used in my games.

## Licence
The [licence](https://github.com/BigETI/unity-utils/blob/master/LICENCE.md) for this package is located at https://github.com/BigETI/unity-utils/blob/master/LICENCE.md

## Downloads
Check out the package [releases](https://github.com/BigETI/unity-utils/releases) at https://github.com/BigETI/unity-utils/releases
